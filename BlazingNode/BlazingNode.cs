﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class BlazingNode
    {
        object mContext;
        Document mDocument;

        public bool IsLoaded { get { return mDocument != null; } }
        public Document Document { get { return mDocument; } }
        public List<IParameter> ParametersList { get { return mDocument.ParametersList; } }
        public object Context { get { return mContext; } }


        public void Load(string resourcePath)
        {
            Load(BlazingNodeHelper.LoadFileFromResources(resourcePath));
        }

        public void Load(Document document)
        {
            mDocument = document;
        }

        public void Start(MonoBehaviour mono, object context, bool debugMode = false)
        {
            mContext = context;

            mDocument.Init(context);

            if (debugMode)
                mono.StartCoroutine(_Start(mDocument.RootNode));
            else
                mono.StartCoroutine(BlazingNodeHelper.ToFixedCoroutine(_Start(mDocument.RootNode)));
        }

        public void Teminate(MonoBehaviour mono)
        {
            mono.StopAllCoroutines();

            mDocument.Dispose();
        }

        IEnumerator _Start(INode node)
        {
            yield return node.Execute(null);
        }
    }
}
