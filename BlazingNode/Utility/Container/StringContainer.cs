﻿using UnityEngine;
using System;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class StringContainer
    {
        public string Value;
        public Func<string> ValueFunc;


        public StringContainer()
        {
        }

        public StringContainer(string value)
        {
            this.Value = value;
        }

        public StringContainer(Func<string> valueFunc)
        {
            this.ValueFunc = valueFunc;
        }
    }
}
