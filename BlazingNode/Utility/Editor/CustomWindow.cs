﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class CustomWindow : EditorWindow
    {
        public Action OnGUIRefreshCB { get; set; }
        public Action OnExitCB { get; set; }


        void OnDestroy()
        {
            if (OnExitCB != null)
                OnExitCB();
        }

        void Update()
        {
            Repaint();
        }

        void OnGUI()
        {
            if (OnGUIRefreshCB != null)
                OnGUIRefreshCB();
        }
    }
}
