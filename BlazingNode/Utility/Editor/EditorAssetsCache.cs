﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Hont.BlazingNodePackage
{
    public class EditorAssetsCache
    {
        public static Texture LogoTex;
        public static Texture GridTex;
        public static Material GLMaterial;


        static EditorAssetsCache()
        {
            var path = _BlazingNodeAssetsLocation.GetAssetsPath();

            LogoTex = AssetDatabase.LoadAssetAtPath<Texture>(path + "/LOGO.jpg");
            GridTex = AssetDatabase.LoadAssetAtPath<Texture>(path + "/Grid.png");
            GLMaterial = AssetDatabase.LoadAssetAtPath<Material>(path + "/GLMat.mat");
        }
    }
}
