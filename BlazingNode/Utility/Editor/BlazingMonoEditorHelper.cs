﻿using UnityEditor;
using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public static class BlazingMonoEditorHelper
    {
        public static Color SelectionDocumentColor
        {
            get { return Color.Lerp(GUI.color, Color.red, 0.2f); }
        }

        public static Color SelectionNodeColor
        {
            get { return Color.Lerp(GUI.color, Color.red, 0.5f); }
        }

        public static Color RootNodeColor
        {
            get { return Color.Lerp(GUI.color, new Color(255f / 255f, 132f / 255f, 0), 0.6f); }
        }

        public static Color CopyHightlightColor
        {
            get { return Color.Lerp(GUI.color, new Color(246 / 255f, 197 / 255f, 103 / 255f), 0.6f); }
        }

        public static Color ArgumentBindNodeColor
        {
            get { return Color.Lerp(GUI.color, Color.gray, 0.4f); }
        }

        public static void DrawBezier(Vector2 pos1, Vector2 pos2, bool leftArcOrRight = true)
        {
            var right = leftArcOrRight ? Vector2.left : Vector2.right;
            var t1 = pos1 + right * Vector2.Distance(pos1, pos2) * 0.4f;
            var t2 = pos2 - right * Vector2.Distance(pos1, pos2) * 0.4f;
            Handles.DrawBezier(pos1, pos2, t1, t2, Color.white, null, 4);
        }

        public static void DrawBezierDirection(Vector2 pos1, Vector2 pos2, float time, Color color, bool leftArcOrRight = true, bool invertDirection = false)
        {
            var right = leftArcOrRight ? Vector2.left : Vector2.right;
            var t1 = pos1 + right * Vector2.Distance(pos1, pos2) * 0.4f;
            var t2 = pos2 - right * Vector2.Distance(pos1, pos2) * 0.4f;

            time = 1 - ((time % 3)) / 3f;

            time = invertDirection ? 1 - time : time;

            time -= 0.025f;
            var p1 = Vector3.Lerp(Vector3.Lerp(Vector3.Lerp(pos2, t2, time), Vector3.Lerp(t2, t1, time), time),
                                      Vector3.Lerp(Vector3.Lerp(t2, t1, time), Vector3.Lerp(t1, pos1, time), time), time);
            time += 0.05f;
            var p2 = Vector3.Lerp(Vector3.Lerp(Vector3.Lerp(pos2, t2, time), Vector3.Lerp(t2, t1, time), time),
                                      Vector3.Lerp(Vector3.Lerp(t2, t1, time), Vector3.Lerp(t1, pos1, time), time), time);

            var c = color;
            c = Color.Lerp(c, Color.white, 0.5f);
            var oldColor = Handles.color;
            Handles.color = c;

            Handles.DrawAAPolyLine(5, p1, p2);

            Handles.color = oldColor;
        }
    }
}
