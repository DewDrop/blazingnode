﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hont.BlazingNodePackage
{
    public class MessageManager
    {
        Dictionary<string, Action> mDict;


        public MessageManager()
        {
            mDict = new Dictionary<string, Action>();
        }

        public void Subscribe(string message, Action task)
        {
            if (mDict.ContainsKey(message))
            {
                mDict[message] += task;
            }
            else
            {
                mDict.Add(message, task);
            }
        }

        public void Unsubscribe(string message, Action task)
        {
            if (mDict.ContainsKey(message))
            {
                mDict[message] -= task;

                if (mDict[message] == null)
                {
                    mDict.Remove(message);
                }
            }
        }

        public void Dispatch(string message)
        {
            if (mDict.ContainsKey(message))
            {
                mDict[message]();
                return;
            }
        }
    }
}
