﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Xml.Serialization;

namespace Hont.BlazingNodePackage
{
    public class CoroutineParam : Parameter
    {
        [XmlIgnore]
        public override object Value { get; set; }

        public override string Category { get { return "Variable"; } }
        public override string DisplayType { get { return "Coroutine"; } }


        public CoroutineParam()
            : base()
        {
            Value = default(Coroutine);
        }

        public override void OnDrawFieldGUI(Rect rect)
        {
#if UNITY_EDITOR
            rect.size = new Vector2(rect.size.x, rect.size.y * 0.8f);
            EditorGUI.LabelField(rect, "N/A");
#endif
        }

        public override INode OnInstance(INode[] templatesArray)
        {
            var target = templatesArray.FirstOrDefault(m => m.Name == "Coroutine");

            var result = target.Clone() as INode;
            result.LinkedParameter = this;

            return result;
        }
    }
}
