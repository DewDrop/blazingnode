﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System;
using System.Linq;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class IntParam : Parameter
    {
        public override string Category { get { return "Variable"; } }
        public override string DisplayType { get { return "Int"; } }


        public IntParam()
            : base()
        {
            Value = 0;
        }

        public override void OnDrawFieldGUI(Rect rect)
        {
#if UNITY_EDITOR
            rect.size = new Vector2(rect.size.x, rect.size.y * 0.8f);
            Value = EditorGUI.IntField(rect, (int)Value);
#endif
        }

        public override INode OnInstance(INode[] templatesArray)
        {
            var target = templatesArray.FirstOrDefault(m => m.Name == "Int");

            var result = target.Clone() as INode;
            result.LinkedParameter = this;

            return result;
        }
    }
}
