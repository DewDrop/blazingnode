﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Xml.Serialization;

namespace Hont.BlazingNodePackage
{
    public class Vector3Param : Parameter
    {
        public Vector3 SerializeValue;

        [XmlIgnore]
        public override object Value
        {
            get { return SerializeValue; }
            set { SerializeValue = (Vector3)value; }
        }

        public override string Category { get { return "Variable"; } }
        public override string DisplayType { get { return "Vector3"; } }


        public Vector3Param()
            : base()
        {
            Value = Vector3.zero;
        }

        public override void OnDrawFieldGUI(Rect rect)
        {
#if UNITY_EDITOR
            rect.size = new Vector2(rect.size.x, rect.size.y * 0.8f);
            Value = EditorGUI.Vector3Field(rect, "", (Vector3)Value);
#endif
        }

        public override INode OnInstance(INode[] templatesArray)
        {
            var target = templatesArray.FirstOrDefault(m => m.Name == "Vector3");

            var result = target.Clone() as INode;
            result.LinkedParameter = this;

            return result;
        }
    }
}
