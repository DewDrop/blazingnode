﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Hont.BlazingNodePackage
{
    [Ignore]
    public sealed class NodeGroupOut : Node
    {
        public override string Category { get { return "System"; } }
        public override string Name { get { return "Node Group Out"; } }

        [XmlIgnore]
        public Func<IEnumerator> OnExecuteFunc;


        public NodeGroupOut()
        {
            mInPortList = new List<Port>(20);
            mOutPortList = new List<Port>(20);
        }

        protected override void Init()
        {
            base.Init();

            mOutPortList.Clear();
        }

        public override IEnumerator Execute(Port port)
        {
            yield return OnExecuteFunc();
        }
    }
}
