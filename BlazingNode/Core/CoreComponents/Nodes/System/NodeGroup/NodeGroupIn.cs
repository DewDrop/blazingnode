﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    [Ignore]
    public sealed class NodeGroupIn : Node
    {
        public override string Category { get { return "System"; } }
        public override string Name { get { return "Node Group In"; } }


        public NodeGroupIn()
        {
            mInPortList = new List<Port>(20);
            mOutPortList = new List<Port>(20);
        }

        protected override void Init()
        {
            base.Init();

            mInPortList.Clear();
        }
    }
}
