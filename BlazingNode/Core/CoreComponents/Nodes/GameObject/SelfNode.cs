﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class SelfNode : Node
    {
        public enum TypeEnum { GameObject, Transform }

        public TypeEnum Type;

        public override string Name { get { return "Self"; } }
        public override string Category { get { return "GameObject"; } }
        public override Vector2 Size { get { return new Vector2(90, 40); } }


        protected override void Init()
        {
            base.Init();

            mInPortList.Clear();
        }

        public override IEnumerator Execute(Port port)
        {
            var context = Context as StandardContext;

            switch (Type)
            {
                case TypeEnum.GameObject:
                    port.Value = context.GameObject;
                    break;
                case TypeEnum.Transform:
                    port.Value = context.GameObject.transform;
                    break;
            }

            yield return true;
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            Type = (TypeEnum)EditorGUILayout.EnumPopup("Type", Type);
#endif
        }
    }
}
