﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class PlayAnimatorNode : Node
    {
        public string StateName;
        public int Layer = 0;
        public float NormalizedTime = float.NegativeInfinity;
        public float TransitionDuration = 0.2f;
        public float AttachDelay = 0.2f;

        public override Vector2 Size { get { return new Vector2(110, 40); } }
        public override string Name { get { return "PlayAnimator"; } }
        public override string Category { get { return "Animator"; } }


        protected override void Init()
        {
            base.Init();

            mOutPortList.Clear();
        }

        public override IEnumerator Execute(Port port)
        {
            var context = Context as StandardCharacterContext;

            context.Animator.CrossFade(StateName, TransitionDuration, Layer, NormalizedTime);
            context.Animator.Update(0);

            var animLength = context.Animator.GetNextAnimatorStateInfo(Layer).length;
            var totalDelay = animLength + AttachDelay;

            yield return new WaitForSeconds(totalDelay);

            yield return true;
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            StateName = EditorGUILayout.TextField("State Name ", StateName);
            Layer = EditorGUILayout.IntField("Layer", Layer);
            NormalizedTime = EditorGUILayout.FloatField("NormalizedTime", NormalizedTime);
            TransitionDuration = EditorGUILayout.FloatField("Transiton Duration", TransitionDuration);
            AttachDelay = EditorGUILayout.FloatField("Attach Delay", AttachDelay);
#endif
        }
    }
}
