﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class AddNode : Node
    {
        public bool IsLazy;
        public int Count;

        public override string Name { get { return "+"; } }
        public override string Category { get { return "Math"; } }
        public override Vector2 Size { get { return new Vector2(90, 25 + (Count + 1) * 17 - (IsLazy ? 17 : 0)); } }


        public AddNode()
        {
            Count = 2;
        }

        protected override void Init()
        {
            base.Init();

            UpdateInPortList();
        }

        public override void OnAwake(Document document)
        {
        }

        public override IEnumerator Execute(Port port)
        {
            var result = 0f;

            if (IsLazy)
            {
                result += Convert.ToSingle(port.Value);
            }

            for (int i = 1; i < mInPortList.Count; i++)
            {
                yield return ExecutePort(mInPortList[i]);

                result += Convert.ToSingle(mInPortList[i].Value);
            }

            if (mInPortList[1].Value is int)
            {
                mOutPortList[0].Value = (int)result;
            }
            else if (mInPortList[1].Value is float)
            {
                mOutPortList[0].Value = result;
            }

            yield return ExecutePort(mOutPortList[0]);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            EditorGUI.BeginChangeCheck();
            IsLazy = EditorGUILayout.Toggle("IsLazy: ", IsLazy);
            Count = EditorGUILayout.IntField("Count: ", Count);
            if (EditorGUI.EndChangeCheck())
            {
                if (IsLazy)
                {
                    mInPortList.Clear();

                    for (int i = 0; i < Count; i++)
                        mInPortList.Add(new Port() { Host = this, Name = "In" + (1 + i) });
                }
                else
                {
                    if (Count < 2) Count = 2;
                    UpdateInPortList();
                }

                updateUI = true;
            }
#endif
        }

        void UpdateInPortList()
        {
            mInPortList.Clear();

            mInPortList.Add(new Port() { Host = this, Name = "In", IsSpark = true });

            for (int i = 0; i < Count; i++)
            {
                mInPortList.Add(new Port() { Host = this, Name = "In" + (1 + i) });
            }
        }
    }
}
