﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class DivNode : Node
    {
        public int Count;

        public override string Name { get { return "/"; } }
        public override string Category { get { return "Math"; } }
        public override Vector2 Size { get { return new Vector2(90, 25 + (Count + 1) * 17); } }


        public DivNode()
        {
            Count = 2;
        }

        protected override void Init()
        {
            base.Init();

            UpdateInPortList();
        }

        public override void OnAwake(Document document)
        {
        }

        public override IEnumerator Execute(Port port)
        {
            var result = 0f;

            yield return ExecutePort(mInPortList[1]);
            yield return ExecutePort(mInPortList[2]);

            var a = Convert.ToSingle(mInPortList[1].Value);
            var b = Convert.ToSingle(mInPortList[2].Value);

            result = a / b;

            if (mInPortList[1].Value is int)
            {
                mOutPortList[0].Value = (int)result;
            }
            else if (mInPortList[1].Value is float)
            {
                mOutPortList[0].Value = result;
            }

            yield return ExecutePort(mOutPortList[0]);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
        }

        void UpdateInPortList()
        {
            mInPortList.Clear();

            mInPortList.Add(new Port() { Host = this, Name = "In", IsSpark = true });

            for (int i = 0; i < Count; i++)
            {
                mInPortList.Add(new Port() { Host = this, Name = "In" + (1 + i) });
            }
        }
    }
}
