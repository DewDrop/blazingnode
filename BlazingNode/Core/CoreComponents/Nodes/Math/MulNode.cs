﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class MulNode : Node
    {
        public int Count;

        public override string Name { get { return "x"; } }
        public override string Category { get { return "Math"; } }
        public override Vector2 Size { get { return new Vector2(90, 25 + (Count + 1) * 17); } }


        public MulNode()
        {
            Count = 2;
        }

        protected override void Init()
        {
            base.Init();

            UpdateInPortList();
        }

        public override void OnAwake(Document document)
        {
        }

        public override IEnumerator Execute(Port port)
        {
            var result = 1f;

            for (int i = 1; i < mInPortList.Count; i++)
            {
                yield return ExecutePort(mInPortList[i]);

                result *= Convert.ToSingle(mInPortList[i].Value);
            }

            if (mInPortList[1].Value is int)
            {
                mOutPortList[0].Value = (int)result;
            }
            else if (mInPortList[1].Value is float)
            {
                mOutPortList[0].Value = result;
            }

            yield return ExecutePort(mOutPortList[0]);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            EditorGUI.BeginChangeCheck();
            Count = EditorGUILayout.IntField("Count", Count);
            if (EditorGUI.EndChangeCheck())
            {
                if (Count < 2) Count = 2;
                UpdateInPortList();
                updateUI = true;
            }
#endif
        }

        void UpdateInPortList()
        {
            mInPortList.Clear();

            mInPortList.Add(new Port() { Host = this, Name = "In", IsSpark = true });

            for (int i = 0; i < Count; i++)
            {
                mInPortList.Add(new Port() { Host = this, Name = "In" + (1 + i) });
            }
        }
    }
}
