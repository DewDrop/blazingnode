﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class DispatchMessageNode : Node
    {
        public string MessageName;

        public override string Name { get { return "Dispatch Message"; } }
        public override string Category { get { return "Message"; } }
        public override Vector2 Size { get { return new Vector2(120, 40); } }


        public DispatchMessageNode()
        {
        }

        public override void OnAwake(Document document)
        {
        }

        public override IEnumerator Execute(Port port)
        {
            var standardContext = Context as StandardContext;

            standardContext.MessageManager.Dispatch(MessageName);

            yield return base.Execute(port);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            EditorGUI.BeginChangeCheck();
            MessageName = EditorGUILayout.TextField("Message ", MessageName);
            if (EditorGUI.EndChangeCheck())
            {
                updateUI = true;
            }
#endif
        }
    }
}
