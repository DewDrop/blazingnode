﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class IfNode : Node
    {
        public enum CompareEnum { Less, LessThan, Equls, GreateThan, Greate };

        public CompareEnum CompareCondition;

        public override Vector2 Size { get { return new Vector2(100, 72); } }
        public override string Name { get { return "If"; } }
        public override string Category { get { return "Logic"; } }


        protected override void Init()
        {
            base.Init();

            mInPortList.Add(new Port() { Host = this, Name = "A" });
            mInPortList.Add(new Port() { Host = this, Name = "B" });

            mOutPortList.Clear();
            mOutPortList.Add(new Port() { Host = this, Name = "True" });
            mOutPortList.Add(new Port() { Host = this, Name = "False" });
        }

        public override IEnumerator Execute(Port port)
        {
            yield return ExecutePort(mInPortList[1]);
            yield return ExecutePort(mInPortList[2]);

            var comparerResult = Comparer.Default
                .Compare(mInPortList[1].Value, mInPortList[2].Value);

            var result = false;

            switch (CompareCondition)
            {
                case CompareEnum.Less:
                    if (comparerResult < 0) result = true;
                    break;
                case CompareEnum.LessThan:
                    if (comparerResult <= 0) result = true;
                    break;
                case CompareEnum.Equls:
                    if (comparerResult == 0) result = true;
                    break;
                case CompareEnum.GreateThan:
                    if (comparerResult >= 0) result = true;
                    break;
                case CompareEnum.Greate:
                    if (comparerResult > 0) result = true;
                    break;
            }

            if (result)
                yield return ExecutePort(mOutPortList[0]);
            else
                yield return ExecutePort(mOutPortList[1]);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;

#if UNITY_EDITOR
            CompareCondition = (CompareEnum)EditorGUILayout.EnumPopup("Compare Condition", CompareCondition);
#endif
        }
    }
}
