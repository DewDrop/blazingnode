﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class InvertNode : Node
    {
        public bool invert;

        public override string Name { get { return "Invert"; } }
        public override string Category { get { return "Logic"; } }
        public override Vector2 Size { get { return new Vector2(80, 40); } }


        public InvertNode()
        {
            invert = false;
        }

        public override IEnumerator Execute(Port port)
        {
            var value = !(bool)port.Value;

            mOutPortList[0].Value = value;
            yield return ExecutePort(mOutPortList[0]);
        }
    }
}
