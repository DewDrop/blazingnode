﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class IfTrueNode : Node
    {
        public override Vector2 Size { get { return new Vector2(130, 54); } }
        public override string Name { get { return "IfTrue"; } }
        public override string Category { get { return "Logic"; } }


        protected override void Init()
        {
            base.Init();

            mInPortList.Add(new Port() { Host = this, Name = "Compare" });

            mOutPortList.Clear();
            mOutPortList.Add(new Port() { Host = this, Name = "True" });
            mOutPortList.Add(new Port() { Host = this, Name = "False" });
        }

        public override IEnumerator Execute(Port port)
        {
            if (((bool)mInPortList[0].Value) == true)
                yield return ExecutePort(mOutPortList[0]);
            else
                yield return ExecutePort(mOutPortList[1]);
        }
    }
}
