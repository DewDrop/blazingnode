﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class IsNullNode : Node
    {
        public override Vector2 Size { get { return new Vector2(130, 54); } }
        public override string Name { get { return "IsNull"; } }
        public override string Category { get { return "Logic"; } }


        protected override void Init()
        {
            base.Init();

            mInPortList.Add(new Port() { Host = this, Name = "Compare" });

            mOutPortList.Clear();
            mOutPortList.Add(new Port() { Host = this, Name = "True" });
            mOutPortList.Add(new Port() { Host = this, Name = "False" });
        }

        public override IEnumerator Execute(Port port)
        {
            yield return ExecutePort(mInPortList[1]);

            if (mInPortList[1].Value == null)
                yield return ExecutePort(mOutPortList[0]);
            else
                yield return ExecutePort(mOutPortList[1]);
        }
    }
}
