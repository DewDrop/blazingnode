﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System;

namespace Hont.BlazingNodePackage
{
    public class Value_CoroutineNode : Value_BaseNode<Coroutine>
    {
        public override object SeriValue { get { return null; } set { } }
        protected override string InternalName { get { return "Coroutine"; } }

        
        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            if (LinkedParameter != null)
                base.OnDrawAttributesGUI(out updateUI);
            else
                EditorGUILayout.LabelField("N/A");
#endif
        }

        public override void OnEditorUpdate()
        {
        }
    }
}
