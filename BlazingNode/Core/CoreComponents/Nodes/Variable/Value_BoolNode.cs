﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System;

namespace Hont.BlazingNodePackage
{
    public class Value_BoolNode : Value_BaseNode<bool>
    {
        protected override string InternalName { get { return "Bool"; } }


        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            if (LinkedParameter != null)
                base.OnDrawAttributesGUI(out updateUI);
            else
                Value = EditorGUILayout.ToggleLeft("Value ", Value);
#endif
        }
    }
}
