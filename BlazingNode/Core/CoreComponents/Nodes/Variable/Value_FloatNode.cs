﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System;

namespace Hont.BlazingNodePackage
{
    public class Value_FloatNode : Value_BaseNode<float>
    {
        protected override string InternalName { get { return "Float"; } }


        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            if (LinkedParameter != null)
                base.OnDrawAttributesGUI(out updateUI);
            else
                Value = EditorGUILayout.FloatField("Value ", Value);
#endif
        }
    }
}
