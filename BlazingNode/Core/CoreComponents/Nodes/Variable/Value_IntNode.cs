﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class Value_IntNode : Value_BaseNode<int>
    {
        protected override string InternalName { get { return "Int"; } }


        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            if (LinkedParameter != null)
                base.OnDrawAttributesGUI(out updateUI);
            else
                Value = EditorGUILayout.IntField("Value ", Value);
#endif
        }
    }
}
