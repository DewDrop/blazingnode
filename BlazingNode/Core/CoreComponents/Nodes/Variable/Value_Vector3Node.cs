﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System;

namespace Hont.BlazingNodePackage
{
    public class Value_Vector3Node : Value_BaseNode<Vector3>
    {
        protected override string InternalName { get { return "Vector3"; } }
        public override object SeriValue
        {
            get
            {
                return string.Format("{0}_{1}_{2}", Value.x, Value.y, Value.z);
            }

            set
            {
                var temp = value.ToString().Split('_');
                Value = new Vector3(float.Parse(temp[0]), float.Parse(temp[1]), float.Parse(temp[2]));
            }
        }


        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            if (LinkedParameter != null)
                base.OnDrawAttributesGUI(out updateUI);
            else
                Value = EditorGUILayout.Vector3Field("Value ", Value);
#endif
        }
    }
}
