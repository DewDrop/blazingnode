﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Linq;

namespace Hont.BlazingNodePackage
{
    public class GetAroundPointNode : Node
    {
        public float angle = 180;
        public bool angleReferenceForward = true;
        public float innerRadius = 2;
        public float outerRadius = 3;
        public float safeHeight = 1.5f;
        public float raycastLength = 5f;
        public bool isDrawGizmos;

        public override string Name { get { return "GetAroundPoint"; } }
        public override string Category { get { return "Transform"; } }
        public override Vector2 Size { get { return new Vector2(120, 60); } }


        protected override void Init()
        {
            base.Init();

            mInPortList.Add(new Port() { Host = this, Name = "Target" });
            mOutPortList.Add(new Port() { Host = this, Name = "Point" });
        }

        public override IEnumerator Execute(Port port)
        {
            yield return ExecutePort(mInPortList[0]);

            var go = mInPortList[0].Value as GameObject;
            var transform = mInPortList[0].Value as Transform;
            var target = go == null ? transform : go.transform;

            var up = -Physics.gravity.normalized;
            var quat = Quaternion.AngleAxis(Random.Range(-angle, angle), up);
            var forward = Vector3.ProjectOnPlane(target.forward, up);
            var randomPoint = quat * (angleReferenceForward ? forward : -forward);
            randomPoint = randomPoint.normalized * Random.Range(innerRadius, outerRadius);
            randomPoint = target.position + Vector3.ProjectOnPlane(randomPoint, up);

            var aroundPoint = randomPoint + safeHeight * up;

            var hits = Physics.RaycastAll(new Ray(aroundPoint, Physics.gravity.normalized), raycastLength);
            var groundHit = hits.FirstOrDefault(m => m.transform.CompareTag(_Const.TAG_GROUND));

            if (groundHit.transform != null)
            {
                if (isDrawGizmos)
                    Debug.DrawRay(groundHit.point, Vector3.forward * 0.3f, Color.red, 0.5f);

                mOutPortList[1].Value = groundHit.point;
            }

            yield return base.Execute(port);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            angle = EditorGUILayout.FloatField("Angle", angle);
            angleReferenceForward = EditorGUILayout.Toggle("Angle Reference Forward", angleReferenceForward);

            innerRadius = EditorGUILayout.FloatField("Inner Radius", innerRadius);
            outerRadius = EditorGUILayout.FloatField("Outer Radius", outerRadius);
            safeHeight = EditorGUILayout.FloatField("Safe Height", safeHeight);
            raycastLength = EditorGUILayout.FloatField("Raycast Length", raycastLength);
            isDrawGizmos = EditorGUILayout.Toggle("Is Draw Gizmos", isDrawGizmos);
#endif
        }
    }
}
