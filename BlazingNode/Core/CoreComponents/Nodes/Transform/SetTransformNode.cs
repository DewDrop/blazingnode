﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class SetTransformNode : Node
    {
        Transform mTargetTransform;

        public bool ChangeSelf;

        public bool HasNewParent;
        public string ParentGOPath;

        public bool HasNewPosition;
        public bool IsLocalPosition;
        public Vector3 NewPosition;

        public bool HasNewRotation;
        public bool IsLocalRotation;
        public Vector3 NewRotation;

        public bool HasNewScale;
        public Vector3 NewScale;

        public override string Name { get { return "Set Transform"; } }
        public override string Category { get { return "Transform"; } }
        public override Vector2 Size { get { return ChangeSelf ? base.Size : new Vector2(120, 55); } }


        protected override void Init()
        {
            base.Init();

            if (!ChangeSelf)
                mInPortList.Add(new Port() { Host = this, Name = "Transform" });
        }

        public override IEnumerator Execute(Port port)
        {
            if (ChangeSelf)
            {
                mTargetTransform = (Context as StandardContext).GameObject.transform;
            }
            else
            {
                yield return ExecutePort(mInPortList[1]);

                if (mInPortList[1].Value is GameObject)
                    mTargetTransform = (mInPortList[1].Value as GameObject).transform;
                if (mInPortList[1].Value is Transform)
                    mTargetTransform = (mInPortList[1].Value as Transform);
            }

            if (HasNewParent)
            {
                var parentGO = GameObject.Find(ParentGOPath);
                mTargetTransform = parentGO.transform;
            }

            if (HasNewPosition)
            {
                if (IsLocalPosition)
                    mTargetTransform.localPosition = NewPosition;
                else
                    mTargetTransform.position = NewPosition;
            }

            if (HasNewRotation)
            {
                if (IsLocalRotation)
                    mTargetTransform.localEulerAngles = NewRotation;
                else
                    mTargetTransform.eulerAngles = NewRotation;
            }

            if (HasNewScale)
            {
                mTargetTransform.localScale = NewScale;
            }

            yield return base.Execute(port);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR

            EditorGUILayout.BeginHorizontal(GUI.skin.box);
            EditorGUILayout.LabelField("Change Self", GUILayout.Width(170));
            EditorGUI.BeginChangeCheck();
            ChangeSelf = EditorGUILayout.Toggle(ChangeSelf, GUILayout.Width(30));
            if (EditorGUI.EndChangeCheck())
            {
                Init();
                updateUI = true;
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginVertical(GUI.skin.box);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Change", GUILayout.Width(55));
            HasNewParent = EditorGUILayout.Toggle(HasNewParent, GUILayout.Width(30));
            EditorGUILayout.EndHorizontal();

            ParentGOPath = EditorGUILayout.TextField("Parent Path", ParentGOPath);

            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(GUI.skin.box);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Change", GUILayout.Width(55));
            HasNewPosition = EditorGUILayout.Toggle(HasNewPosition, GUILayout.Width(30));

            EditorGUILayout.LabelField("Is Local", GUILayout.Width(55));
            IsLocalPosition = EditorGUILayout.Toggle(IsLocalPosition, GUILayout.Width(30));
            EditorGUILayout.EndHorizontal();

            NewPosition = EditorGUILayout.Vector3Field("Position", NewPosition);

            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Change", GUILayout.Width(55));
            HasNewRotation = EditorGUILayout.Toggle(HasNewRotation, GUILayout.Width(30));

            EditorGUILayout.LabelField("Is Local", GUILayout.Width(55));
            IsLocalRotation = EditorGUILayout.Toggle(IsLocalRotation, GUILayout.Width(30));

            EditorGUILayout.EndHorizontal();

            NewRotation = EditorGUILayout.Vector3Field("Rotation", NewRotation);

            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(GUI.skin.box);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Change", GUILayout.Width(55));
            HasNewScale = EditorGUILayout.Toggle(HasNewScale, GUILayout.Width(30));
            EditorGUILayout.EndHorizontal();

            NewScale = EditorGUILayout.Vector3Field("Scale", NewScale);

            EditorGUILayout.EndVertical();
#endif
        }
    }
}
