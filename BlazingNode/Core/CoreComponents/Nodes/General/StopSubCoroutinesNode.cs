﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class StopSubCoroutinesNode : Node
    {
        public override string Name { get { return "Stop SubCoroutines"; } }
        public override string Category { get { return "General"; } }
        public override Vector2 Size { get { return new Vector2(120, 45); } }


        public StopSubCoroutinesNode()
        {
        }

        protected override void Init()
        {
            base.Init();
        }

        public override IEnumerator Execute(Port port)
        {
            var standardContext = Context as StandardContext;

            foreach (var item in standardContext.SubCoroutinesHashSet)
                standardContext.MonoBehaviour.StopCoroutine(item);

            yield return base.Execute(port);
        }
    }
}
