﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class StopCoroutineNode : Node
    {
        public override string Name { get { return "Stop Coroutine"; } }
        public override string Category { get { return "General"; } }
        public override Vector2 Size { get { return new Vector2(120, 55); } }


        public StopCoroutineNode()
        {
        }

        protected override void Init()
        {
            base.Init();

            mInPortList.Add(new Port() { Host = this, Name = "Coroutine" });
        }

        public override IEnumerator Execute(Port port)
        {
            yield return ExecutePort(mInPortList[1]);

            var coroutine = mInPortList[1].Value as Coroutine;

            if (coroutine != null)
            {
                var context = Context as StandardContext;
                context.MonoBehaviour.StopCoroutine(coroutine);
            }

            yield return base.Execute(port);
        }
    }
}
