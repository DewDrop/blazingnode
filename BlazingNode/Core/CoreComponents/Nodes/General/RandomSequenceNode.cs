﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class RandomSequenceNode : Node
    {
        public int OutNodeCount;
        List<Port> mCacheOutPortList;

        public override string Name { get { return "Random Sequence"; } }
        public override string Category { get { return "General"; } }
        public override Vector2 Size { get { return new Vector2(120, 25 + OutNodeCount * 17); } }


        public RandomSequenceNode()
        {
            OutNodeCount = 1;
            mCacheOutPortList = new List<Port>(10);
        }

        protected override void Init()
        {
            base.Init();

            UpdateOutPortList();
        }

        public override void OnAwake(Document document)
        {
        }

        public override IEnumerator Execute(Port port)
        {
            mCacheOutPortList.Clear();
            mCacheOutPortList.AddRange(mOutPortList);

            //--------------
            for (int i = 0; i < mCacheOutPortList.Count; i++)
            {
                var randomIndex = UnityEngine.Random.Range(0, mCacheOutPortList.Count);

                var temp = mCacheOutPortList[i];
                mCacheOutPortList[i] = mCacheOutPortList[randomIndex];
                mCacheOutPortList[randomIndex] = temp;
            }
            //--------------Random

            for (int i = 0; i < mCacheOutPortList.Count; i++)
            {
                var item = mCacheOutPortList[i];

                item.Value = mInPortList[0].Value;

                yield return ExecutePort(item);
            }

            yield return true;
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            EditorGUI.BeginChangeCheck();
            OutNodeCount = EditorGUILayout.IntField("Out Node Count", OutNodeCount);
            if (EditorGUI.EndChangeCheck())
            {
                UpdateOutPortList();
                updateUI = true;
            }
#endif
        }

        void UpdateOutPortList()
        {
            mOutPortList.Clear();

            for (int i = 0; i < OutNodeCount; i++)
            {
                mOutPortList.Add(new Port() { Host = this, Name = "Out" + i });
            }
        }
    }
}
