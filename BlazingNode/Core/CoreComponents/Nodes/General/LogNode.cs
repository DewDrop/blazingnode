﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class LogNode : Node
    {
        public enum TypeEnum { Log, Warning, Error }
        public bool ShowLog = true;
        public bool ShowFrameCount = false;
        public TypeEnum Type;
        public string LogContent;

        public override string Name { get { return "Log"; } }
        public override string Category { get { return "General"; } }


        public LogNode()
        {
            LogContent = "";
        }

        public override IEnumerator Execute(Port port)
        {
            var finalLog = LogContent;

            if (ShowFrameCount)
                finalLog += "   FrameCount: " + Time.frameCount;

            if (ShowLog)
            {
                switch (Type)
                {
                    case TypeEnum.Log:
                        Debug.Log(finalLog);
                        break;
                    case TypeEnum.Warning:
                        Debug.LogWarning(finalLog);
                        break;
                    case TypeEnum.Error:
                        Debug.LogError(finalLog);
                        break;
                }
            }

            mOutPortList[0].Value = port.Value;

            yield return ExecutePort(mOutPortList[0]);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            ShowLog = EditorGUILayout.Toggle("Show Log", ShowLog);
            ShowFrameCount = EditorGUILayout.Toggle("Show Fram Count", ShowFrameCount);
            Type = (TypeEnum)EditorGUILayout.EnumPopup("Type", Type);
            EditorGUILayout.LabelField("Log Content");
            LogContent = EditorGUILayout.TextArea(LogContent);
#endif
        }
    }
}
