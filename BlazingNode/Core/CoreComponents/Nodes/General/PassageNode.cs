﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class PassageNode : Node
    {
        public override string Name { get { return "Passage"; } }
        public override string Category { get { return "General"; } }
        public override Vector2 Size { get { return new Vector2(100, 58); } }


        public PassageNode()
        {
        }

        protected override void Init()
        {
            base.Init();

            mInPortList.Add(new Port() { Host = this, Name = "Off" });
        }

        public override IEnumerator Execute(Port port)
        {
            yield return ExecutePort(mInPortList[1]);

            var off = (bool)mInPortList[1].Value;

            if (off)
                yield return true;
            else
                yield return base.Execute(port);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
#endif
        }
    }
}
