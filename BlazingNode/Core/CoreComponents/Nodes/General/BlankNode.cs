﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class BlankNode : Node
    {
        public override string Name { get { return "Blank"; } }
        public override string Category { get { return "General"; } }


        public BlankNode()
        {
        }

        public override IEnumerator Execute(Port port)
        {
            yield return base.Execute(port);
        }
    }
}
