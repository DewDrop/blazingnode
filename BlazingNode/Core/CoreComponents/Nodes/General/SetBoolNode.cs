﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class SetBoolNode : Node
    {
        public bool Value;

        public override string Name { get { return "Set Bool"; } }
        public override string Category { get { return "General"; } }
        public override Vector2 Size { get { return new Vector2(90, 42); } }


        public SetBoolNode()
        {
        }

        public override IEnumerator Execute(Port port)
        {
            mOutPortList[0].Value = Value;

            yield return ExecutePort(mOutPortList[0]);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;

#if UNITY_EDITOR
            Value = EditorGUILayout.Toggle("Value", Value);
#endif
        }
    }
}
