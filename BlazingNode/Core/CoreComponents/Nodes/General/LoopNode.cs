﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class LoopNode : Node
    {
        bool mIsFirstFrame = true;

        public override string Name { get { return "Loop"; } }
        public override string Category { get { return "General"; } }
        public override Vector2 Size { get { return new Vector2(100, 40); } }


        public LoopNode()
        {
        }

        public override IEnumerator Execute(Port port)
        {
            while (true)
            {
                if (!mIsFirstFrame)
                    yield return null;

                mIsFirstFrame = false;

                yield return base.Execute(port);
            }
        }
    }
}
