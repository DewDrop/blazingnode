﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class OnceNode : Node
    {
        bool mIsTriggered;

        public override string Name { get { return "Once"; } }
        public override string Category { get { return "General"; } }


        public override void OnAwake(Document document)
        {
            base.OnAwake(document);

            mIsTriggered = false;
        }

        public override IEnumerator Execute(Port port)
        {
            if (mIsTriggered)
            {
                yield return null;
            }
            else
            {
                mIsTriggered = true;

                yield return base.Execute(port);
            }
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
#endif
        }
    }
}
