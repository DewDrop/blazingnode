﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class RandomSwitchNode : Node
    {
        public int OutNodeCount;

        public override string Name { get { return "Random Switch"; } }
        public override string Category { get { return "General"; } }
        public override Vector2 Size { get { return new Vector2(120, 25 + OutNodeCount * 17); } }


        public RandomSwitchNode()
        {
            OutNodeCount = 1;
        }

        protected override void Init()
        {
            base.Init();

            UpdateOutPortList();
        }

        public override void OnAwake(Document document)
        {
        }

        public override IEnumerator Execute(Port port)
        {
            var randomIndex = Random.Range(0, mOutPortList.Count);
            yield return ExecutePort(mOutPortList[randomIndex]);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            EditorGUI.BeginChangeCheck();
            OutNodeCount = EditorGUILayout.IntField("Out Node Count", OutNodeCount);
            if (EditorGUI.EndChangeCheck())
            {
                UpdateOutPortList();
                updateUI = true;
            }
#endif
        }

        void UpdateOutPortList()
        {
            mOutPortList.Clear();

            for (int i = 0; i < OutNodeCount; i++)
            {
                mOutPortList.Add(new Port() { Host = this, Name = "Out" + i });
            }
        }
    }
}
