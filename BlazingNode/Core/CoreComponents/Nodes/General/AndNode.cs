﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class AndNode : Node
    {
        public int count;

        public override string Name { get { return "&&"; } }
        public override string Category { get { return "General"; } }
        public override Vector2 Size { get { return new Vector2(90, 25 + count * 17); } }


        public AndNode()
        {
            count = 1;
        }

        protected override void Init()
        {
            base.Init();

            UpdatePortList();
        }

        public override IEnumerator Execute(Port port)
        {
            var result = false;

            for (int i = 0; i < mInPortList.Count; i++)
            {
                var item = mInPortList[i];

                yield return ExecutePort(item);

                result &= (bool)item.Value;
            }

            port.Value = result;

            yield return true;
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
            EditorGUI.BeginChangeCheck();
            count = EditorGUILayout.IntField("Count", count);
            if (EditorGUI.EndChangeCheck())
            {
                UpdatePortList();
                updateUI = true;
            }
#endif
        }

        void UpdatePortList()
        {
            mInPortList.Clear();

            for (int i = 0; i < count; i++)
            {
                mInPortList.Add(new Port() { Host = this, Name = "In" + i });
            }
        }
    }
}
