﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class SetGameObjectNode : Node
    {
        public string GameObjectPath;

        public override string Name { get { return "Set GameObject"; } }
        public override string Category { get { return "General"; } }
        public override Vector2 Size { get { return new Vector2(120, 42); } }


        public SetGameObjectNode()
        {
        }

        public override IEnumerator Execute(Port port)
        {
            var go = GameObject.Find(GameObjectPath);
            mOutPortList[0].Value = go;

            yield return ExecutePort(mOutPortList[0]);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;

#if UNITY_EDITOR
            GameObjectPath = EditorGUILayout.TextField("Path", GameObjectPath);
#endif
        }
    }
}
