﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class DelayNode : Node
    {
        public override string Name { get { return "Delay"; } }
        public override string Category { get { return "General"; } }
        public override Vector2 Size { get { return new Vector2(90, 55); } }


        public DelayNode()
        {
        }

        protected override void Init()
        {
            base.Init();

            mInPortList.Add(new Port() { Host = this, Name = "Inteval" });

            mOutPortList[0].Name = "Out";
        }

        public override IEnumerator Execute(Port port)
        {
            yield return ExecutePort(mInPortList[1]);

            var interval = (float)mInPortList[1].Value;
            yield return new WaitForSeconds(interval);

            yield return base.Execute(port);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR
#endif
        }
    }
}
