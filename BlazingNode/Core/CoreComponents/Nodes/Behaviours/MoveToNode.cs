﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class MoveToNode : Node
    {
        public override Vector2 Size { get { return new Vector2(140, 60); } }
        public override string Name { get { return "MoveTo"; } }
        public override string Category { get { return "Behaviours"; } }


        protected override void Init()
        {
            base.Init();

            mInPortList.Add(new Port() { Host = this, Name = "Goal" });
        }

        public override IEnumerator Execute(Port port)
        {
            yield return ExecutePort(mInPortList[1]);

            var standardContext = Context as StandardContext;
            var self = standardContext.GameObject.transform;
            var goal = (Vector3)mInPortList[1].Value;

            for (int i = 0; i < 20; i++)
            {
                self.position = Vector3.MoveTowards(self.position, goal, Time.deltaTime * 20f);
                yield return null;
            }

            yield return base.Execute(port);
        }

        public override void OnDrawAttributesGUI(out bool updateUI)
        {
            updateUI = false;
#if UNITY_EDITOR

#endif
        }
    }
}
