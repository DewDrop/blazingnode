﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class StandardContext
    {
        HashSet<Coroutine> mSubCoroutinesHashSet;
        MessageManager mMessageManager;

        public GameObject GameObject;
        public MonoBehaviour MonoBehaviour;

        public MessageManager MessageManager
        {
            get
            {
                if (mMessageManager == null)
                    mMessageManager = new MessageManager();

                return mMessageManager;
            }
        }

        public HashSet<Coroutine> SubCoroutinesHashSet
        {
            get
            {
                if (mSubCoroutinesHashSet == null)
                    mSubCoroutinesHashSet = new HashSet<Coroutine>();

                return mSubCoroutinesHashSet;
            }
        }
    }
}
