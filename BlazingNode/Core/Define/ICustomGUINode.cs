﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public interface ICustomGUINode
    {
        void DrawNode(INode node, bool isSelection, Color color);
        void DrawPort(Rect nodeRect, INode node, int index, bool isInPort);

        Rect GetNodeRect(INode node);
        Rect GetPortRect(Rect nodeRect, INode node, int index, bool isInPort);
    }
}
