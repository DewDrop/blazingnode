﻿using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public interface IGUIDObject
    {
        long GUID { get; set; }
    }
}
