﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public interface INode : ICloneable, IGUINode, IGUIDObject, IBlazingObject
    {
        string Category { get; }
        string Name { get; }
        string Remark { get; set; }

        Graph CurrentGraph { get; set; }
        Graph SubGraph { get; set; }

        IParameter LinkedParameter { get; set; }
        List<Port> InPortList { get; }
        List<Port> OutPortList { get; }

        IEnumerator Execute(Port port);

        void OnAwake(Document document);
        void OnDestroy();
        void OnDeserialize();
    }
}
