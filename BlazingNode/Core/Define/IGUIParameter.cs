﻿using UnityEngine;
using System;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public interface IGUIParameter
    {
        INode OnInstance(INode[] templatesArray);
        void OnDrawFieldGUI(Rect rect);
    }
}
