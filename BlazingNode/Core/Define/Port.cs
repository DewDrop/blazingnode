﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Hont.BlazingNodePackage
{
    public class Port : IGUIDObject
    {
        /// <summary>
        /// Can`t need manual set.
        /// </summary>
        public long GUID { get; set; }

        public bool IsSpark { get; set; }

        /// <summary>
        /// When you create, you need to set this value of it.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// When you create, you need to set this value of it.
        /// </summary>
        [XmlIgnore]
        public INode Host { get; set; }

        /// <summary>
        /// Dynamic Value.
        /// </summary>
        [XmlIgnore]
        public Port ConnectPort { get; set; }

        /// <summary>
        /// Dynamic Value.
        /// </summary>
        [XmlIgnore]
        public object Value { get; set; }


        public Port()
        {
            GUID = BlazingNodeHelper.CreateGUID();
        }
    }
}
