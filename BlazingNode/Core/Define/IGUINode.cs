﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public interface IGUINode
    {
        Vector2 Position { get; set; }
        Vector2 Size { get; }

        void OnEditorUpdate();
        void OnDrawAttributesGUI(out bool updateUI);
    }
}
