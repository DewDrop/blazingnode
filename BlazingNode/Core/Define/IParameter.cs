﻿using UnityEngine;
using System;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public interface IParameter : IGUIDObject, IGUIParameter, ICloneable, IBlazingObject
    {
        string Category { get; }
        string DisplayType { get; }
        string Name { get; set; }
        object Value { get; set; }

        void OnAwake(Document document);
        void OnDestroy();
    }
}
