﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class Document : ICloneable
    {
        public string Name;
        public INode RootNode;
        public List<IParameter> ParametersList;

        public Graph RootGraph { get { return RootNode.SubGraph; } }


        public void Init(object context)
        {
            for (int i = 0; i < RootGraph.NodeList.Count; i++)
            {
                var item = RootGraph.NodeList[i];

                item.Context = context;
                item.OnAwake(this);
            }

            for (int i = 0; i < ParametersList.Count; i++)
            {
                var item = ParametersList[i];
                item.Context = context;
                item.OnAwake(this);
            }
        }

        public void Dispose()
        {
            for (int i = 0; i < RootGraph.NodeList.Count; i++)
            {
                var item = RootGraph.NodeList[i];

                item.OnDestroy();
            }

            for (int i = 0; i < ParametersList.Count; i++)
            {
                var item = ParametersList[i];
                item.OnDestroy();
            }
        }

        public object Clone()
        {
            return base.MemberwiseClone();
        }
    }
}
