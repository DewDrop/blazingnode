﻿using UnityEngine;
using System.Collections;
using System;

namespace Hont.BlazingNodePackage
{
    public abstract class Parameter : IParameter
    {
        public abstract string Category { get; }
        public abstract string DisplayType { get; }

        public virtual long GUID { get; set; }
        public virtual string Name { get; set; }
        public virtual object Value { get; set; }

        public object Context { get; set; }

        protected Document mCurrentDocument;


        public Parameter()
        {
            GUID = BlazingNodeHelper.CreateGUID();
        }

        public abstract void OnDrawFieldGUI(Rect rect);

        public abstract INode OnInstance(INode[] templatesArray);

        public object Clone()
        {
            var result = base.MemberwiseClone() as IParameter;
            result.GUID = BlazingNodeHelper.CreateGUID();

            return result;
        }

        public virtual void OnAwake(Document document)
        {
            mCurrentDocument = document;
        }

        public virtual void OnDestroy()
        {
        }
    }
}
