﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class Graph
    {
        public long GUID;

        public INode ParentNode;
        public INode InNode;
        public INode OutNode;
        public List<INode> NodeList;


        public Graph()
        {
            GUID = BlazingNodeHelper.CreateGUID();
            NodeList = new List<INode>();
        }

        public static Graph CreateNewRootGraph(NodeGroup parent = null)
        {
            var graph = CreateNewGraph(parent);

            graph.NodeList.Remove(graph.OutNode);
            graph.OutNode = null;

            return graph;
        }

        public static Graph CreateNewGraph(NodeGroup parent = null)
        {
            var result = new Graph();

            result.ParentNode = parent;

            var groupIn = new NodeGroupIn();
            var groupOut = new NodeGroupOut();

            groupIn.CurrentGraph = result;
            groupOut.CurrentGraph = result;

            result.InNode = groupIn;
            result.OutNode = groupOut;

            result.NodeList.Add(groupIn);
            result.NodeList.Add(groupOut);

            parent.SyncSubNodes(groupIn, groupOut);

            return result;
        }
    }
}
