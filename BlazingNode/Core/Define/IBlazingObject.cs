﻿using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public interface IBlazingObject
    {
        object Context { get; set; }
    }
}
