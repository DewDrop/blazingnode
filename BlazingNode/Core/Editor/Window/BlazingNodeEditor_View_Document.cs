﻿using UnityEditor;
using UnityEngine;
using System.Linq;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public partial class BlazingNodeEditor_View
    {
        DocumentsVO mDocumentsVO;
        DocumentVO mCurrentDocumentVO;

        public DocumentVO CurrentDocumentVO { get { return mCurrentDocumentVO; } }

        float LeftContentPanelWidth = 220;

        float DocumentCloseButton { get { return 20; } }
        float DocumentTitleBarWidth { get { return 170; } }
        float DocumentTitleBarHeight { get { return 25; } }

        float DocumentOriginY { get { return FileBarHeight + LARGE_SPACING; } }
        float DocumentContentOriginY { get { return DocumentOriginY + DocumentTitleBarHeight; } }

        float ContentPanelOriginX { get { return MID_SPACING; } }
        float ContentPanelOriginY { get { return PathBarOriginY + PathBarHeight + SMALL_SPACING; } }
        float RightContentPanelOriginX { get { return ContentPanelOriginX + LeftContentPanelWidth + MID_SPACING; } }
        float RightContentPanelWidth { get { return Screen.width - (LeftContentPanelWidth + MID_SPACING + SMALL_SPACING * 4); } }
        float ContentPanelHeight { get { return Screen.height - (ContentPanelOriginY + LARGE_SPACING * 4); } }


        public void InitDocument()
        {
            mDocumentsVO = new DocumentsVO()
            {
                Documents = new DocumentVO[]
                {
                    new DocumentVO() { Name = "A1" },
                    new DocumentVO() { Name = "A2" },
                }
            };

            mCurrentDocumentVO = mDocumentsVO.Documents[0];
        }

        public void Update_DocumentsVO(DocumentsVO documents)
        {
            mDocumentsVO = documents;
            mCurrentDocumentVO = mDocumentsVO.Documents.FirstOrDefault(m => m.IsActive);

            Update_DocumentVO(mCurrentDocumentVO);
        }

        public void Update_DocumentVO(DocumentVO document)
        {
            Update_NodeGraphVO(document.Graph);
            Update_ParameterVO(document.Parameter);
        }

        public void Update_ActiveDocumentParameters()
        {
            Update_ParameterVO(mCurrentDocumentVO.Parameter);
        }

        public void Update_ActiveDocumentNodeGraph()
        {
            Update_NodeGraphVO(mCurrentDocumentVO.Graph);
        }

        public void Update_ActiveDocumentNodeGraph(Node updateNode)
        {
            var node = mGraphVO.NodeArray.FirstOrDefault(m => m.Host == updateNode);
            node.Name = new StringContainer(() => updateNode.Name);
            node.Position = updateNode.Position;
        }

        void Refresh_Document()
        {
            GUI.Box(new Rect(SMALL_SPACING, DocumentContentOriginY, Screen.width - SMALL_SPACING * 2, Screen.height - (DocumentContentOriginY + SMALL_SPACING * 8)), "");//Document.
            GUI.Box(new Rect(ContentPanelOriginX, PathBarOriginY, Screen.width - (SMALL_SPACING * 4), PathBarHeight), "");//Path bar.

            Refresh_TitleBar();

            GUI.Box(new Rect(ContentPanelOriginX, ContentPanelOriginY, LeftContentPanelWidth, ContentPanelHeight), "");//Left
            GUI.Box(new Rect(RightContentPanelOriginX, ContentPanelOriginY, RightContentPanelWidth, ContentPanelHeight), "");//Right

            RefreshToolbag();
            Refresh_NodeGraph();
        }

        void Refresh_TitleBar()
        {
            for (int i = 0; i < mDocumentsVO.Documents.Length; i++)
            {
                var item = mDocumentsVO.Documents[i];
                var spacing = DocumentTitleBarWidth + LARGE_SPACING;
                var x = ContentPanelOriginX + spacing * i;

                var oldColor = GUI.color;

                GUI.color = item == mCurrentDocumentVO
                    ? BlazingMonoEditorHelper.SelectionDocumentColor
                    : GUI.color;

                var titleRect = new Rect(x, DocumentOriginY + 1, DocumentTitleBarWidth, DocumentTitleBarHeight);
                GUI.Box(titleRect, item.Name);

                if (mIsMouseUpOnce && titleRect.Contains(mMousePosition))
                {
                    if (item.OnSelectionBtnCB != null)
                        item.OnSelectionBtnCB(item);
                }

                GUI.color = oldColor;

                var isClosed = GUI.Button(new Rect(x + DocumentTitleBarWidth - DocumentCloseButton, DocumentOriginY + 1, DocumentCloseButton, DocumentCloseButton), "X");//Close Button.

                if (isClosed)
                {
                    if (item.OnCloseBtnClickCB != null)
                        item.OnCloseBtnClickCB(item);
                }
            }
        }
    }
}
