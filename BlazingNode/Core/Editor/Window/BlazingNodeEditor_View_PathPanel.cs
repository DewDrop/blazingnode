﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Hont.BlazingNodePackage
{
    public partial class BlazingNodeEditor_View
    {
        float PathBarOriginY { get { return DocumentContentOriginY + SMALL_SPACING; } }
        float PathBarHeight { get { return 20; } }

        Rect PathPanelRect { get { return new Rect(ContentPanelOriginX, PathBarOriginY, Screen.width - ContentPanelOriginX * 2f, PathBarHeight); } }

        NodePathVO[] mPathArray;


        public void Update_PathVO(NodePathVO[] pathArray)
        {
            mPathArray = pathArray;
        }

        public void Refresh_PathPanel()
        {
            if (Event.current.type == EventType.Ignore || Event.current.type == EventType.KeyUp)
                return;

            GUI.Box(PathPanelRect, "");

            var areaRect = PathPanelRect;
            areaRect.x += SMALL_SPACING;
            areaRect.height -= 3;
            areaRect.y += 1;

            GUILayout.BeginArea(areaRect);

            EditorGUILayout.BeginHorizontal();
            for (int i = 0; i < mPathArray.Length; i++)
            {
                var item = mPathArray[i];

                var isClicked = false;
                var width = Mathf.Max(50, GUI.skin.label.CalcSize(new GUIContent(item.Name)).x + 20);

                if (i == 0)
                    isClicked = GUILayout.Button(item.Name, "GUIEditor.BreadcrumbLeft", GUILayout.Width(width));
                else
                    isClicked = GUILayout.Button(item.Name, "GUIEditor.BreadcrumbMid", GUILayout.Width(width));

                if (isClicked)
                {
                    if (item.OnClickedCB != null)
                        item.OnClickedCB(item);
                    return;
                }
            }

            EditorGUILayout.EndHorizontal();

            GUILayout.EndArea();
        }
    }
}
