﻿using UnityEngine;
using System.Linq;
using System.Collections;
using UnityEditor;

namespace Hont.BlazingNodePackage
{
    public partial class BlazingNodeEditor_View
    {
        string mLibrarySearchContent;
        bool mIsLibraryEnable;
        bool mCacheIsLibraryEnable;
        Rect mBaseLibraryRect;
        Rect mCurrentLibraryRect;
        Vector2 mLibraryPanelScroll;
        LibraryItemVO[] mLibraryItemVOArray;
        LibraryItemMetaData[] mInternalLibraryItemArray;


        void InitLibrary()
        {
            mBaseLibraryRect = new Rect(0, 0, 200, 400);
            mLibrarySearchContent = "";
            mInternalLibraryItemArray = new LibraryItemMetaData[0];
        }

        public void Update_LibraryVO(LibraryItemVO[] libraryItemVOArray)
        {
            mLibraryItemVOArray = libraryItemVOArray;

            mInternalLibraryItemArray = mLibraryItemVOArray
                .GroupBy(m => m.Category)
                .Select(m => new LibraryItemMetaData() { Category = m.Key, ItemsArray = m.ToArray() })
                .ToArray();
        }

        void Refresh_Library()
        {
            if (mCacheIsLibraryEnable)
            {
                Refresh_LibraryContent();
            }

            DetectLibrayPanelEnable();

            if (mCacheIsLibraryEnable != mIsLibraryEnable)
            {
                //Invoke once.
                if (mIsLibraryEnable)
                {
                    var pos = mMousePosition;
                    mCurrentLibraryRect = new Rect(pos.x, pos.y, mBaseLibraryRect.width, mBaseLibraryRect.height);
                    mLibrarySearchContent = "";

                    for (int i = 0; i < mInternalLibraryItemArray.Length; i++)
                        mInternalLibraryItemArray[i].IsUnfold = false;
                }
                else
                {
                }

                mCacheIsLibraryEnable = mIsLibraryEnable;
            }

            mCacheIsLibraryEnable = mIsLibraryEnable;
        }

        void Refresh_LibraryContent()
        {
            GUI.Box(mCurrentLibraryRect, "");

            var tempRect = mCurrentLibraryRect;
            tempRect.x += 2;
            tempRect.xMax -= 4;
            GUILayout.BeginArea(tempRect);

            mLibraryPanelScroll = EditorGUILayout.BeginScrollView(mLibraryPanelScroll);

            GUILayout.Space(2);

            GUI.SetNextControlName("InputField");
            mLibrarySearchContent = EditorGUILayout.TextField(mLibrarySearchContent);

            for (int i = 0; i < mInternalLibraryItemArray.Length && mIsLibraryEnable; i++)
            {
                var item = mInternalLibraryItemArray[i];

                var hasSearchText = !string.IsNullOrEmpty(mLibrarySearchContent);

                //------Search Filter

                if (hasSearchText
                    && item.ItemsArray.All(m => !m.Name.ToLower().Contains(mLibrarySearchContent.ToLower())))
                {
                    continue;
                }

                //---------------------

                item.IsUnfold = EditorGUILayout.Foldout(item.IsUnfold, item.Category);

                if (item.IsUnfold || hasSearchText)
                {
                    for (int j = 0; j < item.ItemsArray.Length && mIsLibraryEnable; j++)
                    {
                        var libraryItem = item.ItemsArray[j];

                        if (!string.IsNullOrEmpty(mLibrarySearchContent)
                            && !libraryItem.Name.ToLower().Contains(mLibrarySearchContent.ToLower()))
                        {
                            continue;
                        }

                        if (GUILayout.Button(libraryItem.Name))
                        {
                            libraryItem.OnClickedCB(libraryItem, mNodeGraphScrollPosition + (mCurrentLibraryRect.min - NodeGraphRect.min));
                            mIsLibraryEnable = false;
                            break;
                        }
                    }
                }
            }

            EditorGUILayout.EndScrollView();
            GUILayout.EndArea();
        }

        void DetectLibrayPanelEnable()
        {
            if (mIsGridNodeOperationEnable) return;

            var enableFlag = true;
            enableFlag &= RightButtonClicked;
            enableFlag &= !mIsLibraryEnable;
            enableFlag &= NodeGraphRect.Contains(mMousePosition);

            if (enableFlag)
            {
                mIsLibraryEnable = true;
                return;
            }

            var disableFlag = true;
            disableFlag &= mIsMouseUpOnce;
            disableFlag &= !mCurrentLibraryRect.Contains(mMousePosition);
            disableFlag &= mIsLibraryEnable;

            if (disableFlag)
            {
                mIsLibraryEnable = false;
                return;
            }
        }
    }
}
