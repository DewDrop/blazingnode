﻿using UnityEngine;
using System.Collections;
using System;

namespace Hont.BlazingNodePackage
{
    public class LibraryItemMetaData
    {
        public bool IsUnfold;
        public string Category;
        public LibraryItemVO[] ItemsArray;
    }
}
