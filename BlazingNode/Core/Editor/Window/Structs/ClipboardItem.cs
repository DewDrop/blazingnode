﻿using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class ClipboardItem
    {
        public bool IsCut;
        public object Item;
    }
}
