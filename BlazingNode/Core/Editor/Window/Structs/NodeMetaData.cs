﻿using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class NodeMetaData
    {
        public Node Source;
        public Vector2 Position;
    }
}
