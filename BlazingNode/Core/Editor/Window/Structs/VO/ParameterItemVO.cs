﻿using UnityEngine;
using System.Collections;
using System;

namespace Hont.BlazingNodePackage
{
    public class ParameterItemVO
    {
        public string Category;
        public string Type;
        public string Name;
        public object UserData;
        public Action<ParameterItemVO> OnClickedCB;
        public Action<Rect> OnDrawFieldGUICB;
    }
}
