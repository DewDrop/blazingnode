﻿using UnityEngine;
using System.Collections;
using System;

namespace Hont.BlazingNodePackage
{
    public class LibraryItemVO
    {
        public string Category;
        public string Name;
        public object UserData;
        /// <summary>
        /// Clicked VO and mouse position.
        /// </summary>
        public Action<LibraryItemVO, Vector2> OnClickedCB;
    }
}
