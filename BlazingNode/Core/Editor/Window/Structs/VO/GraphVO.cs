﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Hont.BlazingNodePackage
{
    public class GraphVO
    {
        public NodeVO[] NodeArray;
        public PortLinkVO[] PortLinkArray;

        public Action<BlazingNodeEditor_View.PortLink> NodeGraph_OnPortDeleteCB;
        public Action<BlazingNodeEditor_View.PortLink> NodeGraph_OnNewPortLinkedCB;

        /// <summary>
        /// Sender and update dragging position.
        /// </summary>
        public Action<NodeVO, Vector2> NodeGraph_OnDraggingNodeCB;
        public Action<NodeVO, Vector2> NodeGraph_OnDuplicateNodeCB;
        public Action<IEnumerable<NodeVO>> NodeGraph_OnCutNodeCB;
        public Action<IEnumerable<NodeVO>> NodeGraph_OnCopyNodeCB;

        public Action<NodeVO> NodeGraph_OnDoubleClickedNodeCB;

        /// <summary>
        /// Arg1 - Mouse position.
        /// </summary>
        public Action<Vector2> NodeGraph_OnPasteNodeCB;
    }
}
