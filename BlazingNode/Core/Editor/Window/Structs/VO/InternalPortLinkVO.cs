﻿using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class InternalPortLinkVO
    {
        /// <summary>
        /// Correspond with 'OutNode'.
        /// </summary>
        public NodeVO NodeA;
        /// <summary>
        /// Correspond with 'InNode'.
        /// </summary>
        public NodeVO NodeB;
        public int NodeAIndex;
        public int NodeBIndex;
    }
}
