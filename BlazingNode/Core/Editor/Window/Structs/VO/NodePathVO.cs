﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class NodePathVO
    {
        public object UserData;
        public string Name;
        public Action<NodePathVO> OnClickedCB;
    }
}
