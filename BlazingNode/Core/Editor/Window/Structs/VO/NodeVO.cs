﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class NodeVO
    {
        public object Host;
        public StringContainer Name;
        public string Remark;
        public StringContainer[] InPortNameArray;
        public StringContainer[] OutPortNameArray;
        public Vector2 Position;
        public Vector2 Size;
        public bool RootNodeHighligh;
        public bool CopyHightlight;
        public bool NodeIsArgumentBind;

        public ICustomGUINode CustomGUINode;

        public Action<IEnumerable<NodeVO>> OnNodeDeleteCB;
        public Action<NodeVO> OnNodeRemarkCB;
        public Action<NodeVO> OnDrawAttributesGUICB;
        public Action OnEditorUpdateCB;
    }
}
