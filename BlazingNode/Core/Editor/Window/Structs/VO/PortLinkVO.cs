﻿using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class PortLinkVO
    {
        public object HostA;
        public object HostB;
        public int NodeAIndex;
        public int NodeBIndex;
    }
}
