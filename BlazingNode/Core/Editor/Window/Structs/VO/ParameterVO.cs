﻿using UnityEngine;
using System;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class ParameterVO
    {
        public ParameterItemVO[] ParameterVOArray;

        public Action OnCreateParameterCB;
        public Action<string> OnModifyTypeCB;
        public Action<ParameterItemVO, int> OnSwapParameterItemCB;
        public Action<ParameterItemVO, string> OnNameChangeCB;
        public Action<ParameterItemVO, string> OnTypeChangeCB;
        public Action<ParameterItemVO, Vector2> OnDropToNodeGraphCB;
        public Action<ParameterItemVO> OnDeleteCB;
        public string[] ParameterTypesArray;
    }
}
