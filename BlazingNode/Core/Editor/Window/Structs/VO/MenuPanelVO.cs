﻿using UnityEngine;
using System;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class MenuPanelVO
    {
        public Action OnNewFileBtnClickCB;
        public Action OnSaveBtnClickCB;
        public Action OnLoadBtnClickCB;
        public Action OnAboutBtnClickCB;
    }
}
