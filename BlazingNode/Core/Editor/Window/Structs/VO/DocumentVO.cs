﻿using UnityEngine;
using System;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class DocumentVO
    {
        public string Name;

        public bool IsActive;

        public GraphVO Graph;

        public ParameterVO Parameter;

        public object UserData;

        public Action<DocumentVO> OnSelectionBtnCB;
        public Action<DocumentVO> OnCloseBtnClickCB;
    }
}
