﻿#pragma warning disable 0414

using UnityEngine;
using System;
using System.Collections;
using UnityEditor;

namespace Hont.BlazingNodePackage
{
    public partial class BlazingNodeEditor_View
    {
        Action mRepaintCB;

        bool mIsHoldCtrl;
        bool mIsHoldAlt;
        bool mIsHoldShift;

        bool mIsMouseDown;
        bool mLastIsMouseDown;
        bool mIsMouseDownOnce;
        bool mIsMouseUp;
        bool mLastIsMouseUp;
        bool mIsMouseUpOnce;
        bool mIsMouseDoubleClicke;

        int mLastMouseButton;
        float mMouseHoldTimeMark;
        float mMouseHoldTime;
        Vector2 mMousePosition;
        Vector2 mMouseDownPosition;

        bool mCommandChange;
        string mStableCommandName;
        string mLastCommandName;

        bool RightButtonClicked
        {
            get
            {
                var rightClickFlag = false;
                rightClickFlag |= mIsMouseUpOnce && mLastMouseButton == 1;

                return rightClickFlag;
            }
        }


        public void InitGUIEvent(Action repaintCB)
        {
            mRepaintCB = repaintCB;
        }

        public void RefreshEvent()
        {
            if ((Event.current.keyCode == KeyCode.LeftControl || Event.current.keyCode == KeyCode.RightControl)
               && Event.current.type == EventType.KeyDown)
            {
                mIsHoldCtrl = true;

                if (mRepaintCB != null)
                    mRepaintCB();
            }

            else if ((Event.current.keyCode == KeyCode.LeftControl || Event.current.keyCode == KeyCode.RightControl)
                && Event.current.type == EventType.KeyUp)
            {
                mIsHoldCtrl = false;

                if (mRepaintCB != null)
                    mRepaintCB();
            }

            else if ((Event.current.keyCode == KeyCode.LeftAlt || Event.current.keyCode == KeyCode.RightAlt)
                && Event.current.type == EventType.KeyDown)
            {
                mIsHoldAlt = true;

                if (mRepaintCB != null)
                    mRepaintCB();
            }

            else if ((Event.current.keyCode == KeyCode.LeftAlt || Event.current.keyCode == KeyCode.RightAlt)
                && Event.current.type == EventType.KeyUp)
            {
                mIsHoldAlt = false;

                if (mRepaintCB != null)
                    mRepaintCB();
            }

            else if (Event.current.shift && !mIsHoldShift)
            {
                mIsHoldShift = true;

                if (mRepaintCB != null)
                    mRepaintCB();
            }

            else if (!Event.current.shift && mIsHoldShift)
            {
                mIsHoldShift = false;

                if (mRepaintCB != null)
                    mRepaintCB();
            }

            if (Event.current.type == EventType.MouseDown
                || Event.current.type == EventType.MouseUp)
            {
                mIsMouseDown = Event.current.type == EventType.MouseDown;
                mIsMouseUp = Event.current.type == EventType.MouseUp;
            }

            if (!mLastIsMouseDown && mIsMouseDown)
                mIsMouseDownOnce = true;
            else
                mIsMouseDownOnce = false;

            if (!mLastIsMouseUp && mIsMouseUp)
                mIsMouseUpOnce = true;
            else
                mIsMouseUpOnce = false;

            if (mIsMouseDownOnce && mMouseHoldTime > 0f && mMouseHoldTime < 0.2f)
            {
                mIsMouseDoubleClicke = true;
            }
            else
            {
                mIsMouseDoubleClicke = false;
            }

            if (mIsMouseDownOnce)
            {
                mMouseHoldTimeMark = (float)EditorApplication.timeSinceStartup;
                mMouseHoldTime = 0;
            }

            if (mIsMouseUp)
            {
                mMouseHoldTime = (float)EditorApplication.timeSinceStartup - mMouseHoldTimeMark;
            }

            if (mLastCommandName != Event.current.commandName && Event.current.commandName == "")
                mCommandChange = true;
            else
                mCommandChange = false;

            if (!mCommandChange)
                mStableCommandName = Event.current.commandName;

            if (mIsMouseDownOnce)
                mMouseDownPosition = Event.current.mousePosition;

            mLastCommandName = Event.current.commandName;

            mLastIsMouseDown = mIsMouseDown;
            mLastIsMouseUp = mIsMouseUp;

            if (mIsMouseDownOnce)
                mLastMouseButton = Event.current.button;

            mMousePosition = Event.current.mousePosition;
        }

        public void ClearEvent()
        {
            mIsHoldAlt = false;
            mIsHoldCtrl = false;
            mIsHoldShift = false;
            mIsMouseDoubleClicke = false;
            mIsMouseUpOnce = false;
            mIsMouseDown = false;
            mIsMouseDownOnce = false;
        }
    }
}
