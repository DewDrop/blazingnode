﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace Hont.BlazingNodePackage
{
    public partial class BlazingNodeEditor_View
    {
        const float SMALL_SPACING = 3;
        const float MID_SPACING = 5;
        const float LARGE_SPACING = 7;

        float FileBarHeight { get { return 50; } }


        public BlazingNodeEditor_View(Action repaintCB)
        {
            InitDocument();
            InitNodeGraph();
            InitFunctionPanel();
            InitLibrary();
            InitGUIEvent(repaintCB);
        }

        public void Refresh()
        {
            RefreshEvent();

            Refresh_CommonPanel();
            Refresh_MenuPanel();
            Refresh_Document();
            Refresh_Library();
            Refresh_PathPanel();
        }

        void Refresh_CommonPanel()
        {
            GUI.Box(new Rect(0, 0, Screen.width, FileBarHeight), "");//File bar.
        }
    }
}
