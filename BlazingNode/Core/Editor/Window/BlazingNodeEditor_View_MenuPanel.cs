﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Hont.BlazingNodePackage
{
    public partial class BlazingNodeEditor_View
    {
        MenuPanelVO mMenuPanelVO;
        public UnityEngine.Object LoadPathObject { get; set; }


        public void Update_MenuPanelVO(MenuPanelVO menuPanelVO)
        {
            mMenuPanelVO = menuPanelVO;
        }

        void Refresh_MenuPanel()
        {
            GUILayout.BeginArea(new Rect(MID_SPACING, SMALL_SPACING, Screen.width - 4, FileBarHeight - 4));
            GUILayout.BeginHorizontal();

            if (GUILayout.Button(EditorAssetsCache.LogoTex, GUILayout.Height(40), GUILayout.Width(40)))
            {
                EditorUtility.DisplayDialog("About", "Blazing Node Tool\nFree and Open Source\nAuthor:Hont", "OK");
            }

            GUILayout.Button("", new GUIStyle(), GUILayout.Height(40), GUILayout.MinWidth(300));

            if (GUILayout.Button("New", GUILayout.Height(40), GUILayout.Width(150)))
            {
                if (mMenuPanelVO.OnNewFileBtnClickCB != null)
                    mMenuPanelVO.OnNewFileBtnClickCB();
            }

            if (GUILayout.Button("Save", GUILayout.Height(40), GUILayout.Width(150)))
            {
                if (mMenuPanelVO.OnSaveBtnClickCB != null)
                    mMenuPanelVO.OnSaveBtnClickCB();
            }

            EditorGUILayout.BeginHorizontal(GUI.skin.box, GUILayout.MaxWidth(150));
            if (GUILayout.Button("Load", GUILayout.Height(33), GUILayout.MinWidth(140)))
            {
                if (mMenuPanelVO.OnLoadBtnClickCB != null)
                    mMenuPanelVO.OnLoadBtnClickCB();
            }

            EditorGUILayout.BeginVertical();
            GUILayout.Space(8);
            LoadPathObject = EditorGUILayout.ObjectField(LoadPathObject, typeof(UnityEngine.Object), true, GUILayout.Width(40));
            EditorGUILayout.EndVertical();

            GUILayout.EndHorizontal();

            GUILayout.Space(10);

            GUILayout.EndHorizontal();
            GUILayout.EndArea();

            if (mIsHoldCtrl && Event.current.type == EventType.KeyUp && Event.current.keyCode == KeyCode.S)
            {
                if (mMenuPanelVO.OnSaveBtnClickCB != null)
                    mMenuPanelVO.OnSaveBtnClickCB();

                ClearEvent();
            }
        }
    }
}
