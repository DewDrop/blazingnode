﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Hont.BlazingNodePackage
{
    public class _BlazingNodeAssetsLocation : ScriptableObject
    {
        static string mCachePath;


        public static string GetAssetsPath()
        {
            if (!string.IsNullOrEmpty(mCachePath)) return mCachePath;

            var instance = CreateInstance<_BlazingNodeAssetsLocation>();
            var script = MonoScript.FromScriptableObject(instance);

            mCachePath = AssetDatabase.GetAssetPath(script);
            mCachePath = mCachePath.Replace('\\', '/');
            mCachePath = mCachePath.Substring(0, mCachePath.LastIndexOf('/'));

            DestroyImmediate(instance);

            return mCachePath;
        }
    }
}
