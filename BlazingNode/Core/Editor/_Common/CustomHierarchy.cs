﻿using System;
using UnityEditor;
using UnityEngine;

namespace Hont.BlazingNodePackage
{
    [InitializeOnLoad]
    public class CustomHierarchy : ScriptableObject
    {
        static Texture mIcon;


        static CustomHierarchy()
        {
            mIcon = EditorAssetsCache.LogoTex;
            if (mIcon != null)
            {
                EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemOnGUI;
            }
        }

        static void HierarchyWindowItemOnGUI(int instanceID, Rect selectionRect)
        {
            var gameObject = EditorUtility.InstanceIDToObject(instanceID) as GameObject;

            if (gameObject != null && gameObject.GetComponent<BlazingNodeMono>() != null)
            {
                Rect rect = new Rect(selectionRect);
                rect.x = rect.width + (selectionRect.x - 16f);
                rect.width = 16f;
                rect.height = 16f;

                GUI.DrawTexture(rect, mIcon);
            }
        }
    }
}
