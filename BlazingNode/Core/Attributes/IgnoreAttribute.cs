﻿using UnityEngine;
using System.Collections;
using System;

namespace Hont.BlazingNodePackage
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class IgnoreAttribute : Attribute
    {
    }
}
