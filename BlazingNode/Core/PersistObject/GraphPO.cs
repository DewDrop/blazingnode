﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class GraphPO
    {
        public long GUID;

        public long InNodeGUID;
        public long OutNodeGUID;

        public List<NodePO> NodeList;
        public List<PortPO> PortList;


        public GraphPO()
        {
            NodeList = new List<NodePO>();
            PortList = new List<PortPO>();
        }
    }
}
