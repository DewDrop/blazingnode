﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class NodePO
    {
        public SerializableInterface<INode> Node;
        public long LinkedParameterGUID;
        public long SubGraphGUID;
        public List<long> InPortGUIDList;
        public List<long> OutPortGUIDList;


        public NodePO()
        {
            InPortGUIDList = new List<long>();
            OutPortGUIDList = new List<long>();
        }
    }
}
