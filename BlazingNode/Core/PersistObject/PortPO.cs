﻿using UnityEngine;
using System.Collections;

namespace Hont.BlazingNodePackage
{
    public class PortPO
    {
        public long GUID;
        public long HostGUID;
        public long ConnectPortGUID;
    }
}
