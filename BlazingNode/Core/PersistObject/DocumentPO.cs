﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hont.BlazingNodePackage
{
    public class DocumentPO
    {
        public List<GraphPO> GraphPOList;
        public GraphPO RootGraph;
        public SerializableInterface<IParameter>[] ParametersArray;
    }
}
